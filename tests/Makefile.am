# Makefile using z80asm
# Copyright 2007  Bas Wijnen <wijnen@debian.org>
#
# This file is part of z80asm.
#
# Z80asm is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Z80asm is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The output of the assembler can be parsed by vim or emacs.

all: pass patch9377 bug26772 bug47653 bug60151 bug55596

pass: pass.asm pass.correct-err pass.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

patch9377: patch9377.asm patch9377.correct-err patch9377.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

bug26772: bug26772.asm bug26772.correct-err bug26772.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

bug47653: bug47653.asm bug47653.correct-err bug47653.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

bug60151: bug60151.asm bug60151.correct-err bug60151.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

bug55596: bug55596.asm bug55596.correct-err bug55596.correct-bin ../src/z80asm Makefile
	../src/z80asm -I ../headers $< -o $@.bin 2> $@.actual-err
	diff $@.correct-bin $@.bin
	diff $@.correct-err $@.actual-err
	rm $@.bin $@.actual-err

clean:
	rm -f *.actual-err *.bin

.PHONY: clean all pass patch9377
